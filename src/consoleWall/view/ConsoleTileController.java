package consoleWall.view;

import consoleWall.MainApp;
import consoleWall.model.PlatformCommands;
import consoleWall.model.PlatformSettings;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by georg on 29.08.15.
 *
 * Controller for the main view with video game platform tiles as starters
 */
public class ConsoleTileController {
    /* ................................................................................................ ATTRIBUTES .. */

    // FXML Annotations for connecting attributes with according field in FXML file
    @FXML
    private Button buttonSteam;
    @FXML
    private Button buttonUplay;
    @FXML
    private Button buttonOrigin;
    @FXML
    private Button buttonGOG;
    @FXML
    private Button buttonRetroArch;
    @FXML
    private Button buttonXbox;
    @FXML
    private Button buttonArc;
    @FXML
    private Button buttonDesura;
    @FXML
    private Button buttonNES;
    @FXML
    private Button buttonSNES;
    @FXML
    private Button buttonN64;
    @FXML
    private Button buttonWii;

    private Map<String, Button> buttons;    // list of buttons
    private MainApp mainApp;                // Reference to the main application.


    /* .............................................................................................. CONSTRUCTORS .. */
    /**
     * The default constructor is called before the initialize() method.
     */
    public ConsoleTileController() {
        // TODO
    }


    /* ................................................................................................... METHODS .. */
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        this.buttons = new HashMap<String, Button>();
        this.buttons.put("Steam", buttonSteam);
        this.buttons.put("Uplay", buttonUplay);
        this.buttons.put("Origin", buttonOrigin);
        this.buttons.put("GOG", buttonGOG);
        this.buttons.put("RetroArch", buttonRetroArch);
        this.buttons.put("Xbox", buttonXbox);
        this.buttons.put("Arc", buttonArc);
        this.buttons.put("Desura", buttonDesura);
        this.buttons.put("NES", buttonNES);
        this.buttons.put("SNES", buttonSNES);
        this.buttons.put("N64", buttonN64);
        this.buttons.put("Wii", buttonWii);
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        updateButtons();
    }

    /**
     * Used to update the view if settings are changed
     */
    public void updateButtons() {

        // Nintendo Buttons, disable if RetroArch is not available or not set to be used for retro consoles
        if(!mainApp.getAppSettings().isUseRetroArchForConsoles() ||
                mainApp.getSettings().getPlatformSettings("RetroArch").isActive()) {

            buttonNES.setDisable(true);
            buttonSNES.setDisable(true);
            buttonN64.setDisable(true);
        }

        // Set all buttons active/inactive
        for(Map.Entry<String, Button> e : buttons.entrySet())
            this.buttons.get(e.getKey()).disableProperty().setValue(
                    !mainApp.getSettings().getPlatformSettings(e.getKey()).isActive());


        // Nintendo Buttons
        if(mainApp.getAppSettings().isUseRetroArchForConsoles() &&
                mainApp.getSettings().getPlatformSettings("RetroArch").isActive()) {
            buttonNES.setDisable(false);
            buttonSNES.setDisable(false);
            buttonN64.setDisable(false);
        }
    }

    @FXML
    private void startSteam() {
        startGamePlatform("Steam");
    }
    @FXML
    private void startUplay() {
        startGamePlatform("Uplay");
    }
    @FXML
    private void startOrigin() {
        startGamePlatform("Origin");
    }
    @FXML
    private void startGOG() {
        startGamePlatform("GOG");
    }
    @FXML
    private void startNES() {
        startGamePlatform("NES");
    }
    @FXML
    private void startSNES() {
        startGamePlatform("SNES");
    }
    @FXML
    private void startN64() {
        startGamePlatform("N64");
    }
    @FXML
    private void startWii() {
        startGamePlatform("Wii");
    }
    @FXML
    private void startRetroArch() {
        startGamePlatform("RetroArch");
    }
    @FXML
    private void startXbox() {
        startGamePlatform("Xbox");
    }
    @FXML
    private void startArc() {
        startGamePlatform("Arc");
    }
    @FXML
    private void startDesura() {
        startGamePlatform("Desura");
    }



    /**
     * Starts the given video game platform with the command given in the settings
     * @param platform
     */
    private void startGamePlatform(String platform) {
        System.out.println("Starting " + platform + " ...");
        ProcessBuilder pb;
        try {

            PlatformSettings set = mainApp.getSettings().getPlatformSettings(platform);

            // Assemble command
            List<String> command = new ArrayList<String>();
            command.add(set.getPath());

            switch(platform) {
                case "Steam":
                    if(set.getFullscreen())
                        command.add(PlatformCommands.STEAM_FULLSCREEN);
                    else
                        command.add(PlatformCommands.STEAM);
                    break;
                default: break;
            }

            if(mainApp.getAppSettings().isUseRetroArchForConsoles() &&
                    (platform.equals("NES") ||
                            platform.equals("SNES") ||
                            platform.equals("N64"))) {
                command.clear();
                command.add(mainApp.getSettings().getPlatformSettings("RetroArch").getPath());
            }

            pb = new ProcessBuilder(command);
            mainApp.toBack();

            if(platform.equals("Steam") || platform.equals("Uplay") || platform.equals("GOG") || platform.equals("Origin"))
                pb.start();
            else {
                    pb.start().waitFor();
                    mainApp.toFront();
            }


        } catch (Exception e) {
            showMissingExecutableDialog(platform);
            e.printStackTrace();
        }
    }


    /**
     * Shows an error dialog if starting the platform fails
     * @param name
     */
    private void showMissingExecutableDialog(String name) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Executable missing");
        alert.setHeaderText(name + ".exe missing");
        alert.setContentText("Please open settings dialog.");
        alert.showAndWait();
    }
}
