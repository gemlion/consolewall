package consoleWall.view;

import consoleWall.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * Created by georg on 01.09.15.
 */
public class MainAppController {
    // Reference to the main application.
    private MainApp mainApp;
    private boolean inFullscreen = false;

    @FXML
    private Button GSMButton;

    @FXML
    private void initialize() {
        // TODO
    }

    public void handleSettingsButton() {
        mainApp.getPrimaryStage().setFullScreen(false);
        mainApp.showSettingsEditDialog();
    }

    public void handleFullscreenButton() {
        mainApp.getPrimaryStage().setFullScreen(inFullscreen = !inFullscreen);
    }

    public void handleGSMButton() {
        System.out.println("Starting GameSaveManager ...");
        try {
            new ProcessBuilder(mainApp.getSettings().getPlatformSettings("GSM").getPath()).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        this.inFullscreen = mainApp.getAppSettings().isStartInFullscreenMode();
        if(!mainApp.getSettings().getPlatformSettings("GSM").getPath().isEmpty())
            GSMButton.disableProperty().setValue(false);
        else
            GSMButton.disableProperty().setValue(true);
    }
}
