package consoleWall.view;

import consoleWall.MainApp;
import consoleWall.model.AppSettings;
import consoleWall.model.PlatformSettings;
import consoleWall.model.PlatformSettingsWrapper;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.xml.soap.Text;
import java.awt.*;
import java.io.File;
import java.net.URI;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by georg on 30.08.15.
 */
public class SettingsEditDialogController {

    @FXML
    private TextField locationSteam;
    @FXML
    private CheckBox steamFullscreenCheckbox;
    @FXML
    private TextField locationOrigin;
    @FXML
    private TextField locationUplay;
    @FXML
    private TextField locationGOG;
    @FXML
    private TextField locationNES;
    @FXML
    private TextField locationSNES;
    @FXML
    private TextField locationN64;
    @FXML
    private TextField locationWii;
    @FXML
    private TextField locationGSM;
    @FXML
    private TextField locationRetroArch;
    @FXML
    private CheckBox retroArchFullscreenCheckbox;
    @FXML
    private TextField locationXbox;
    @FXML
    private TextField locationArc;
    @FXML
    private TextField locationDesura;
    @FXML
    private javafx.scene.control.Label labelWii;
    @FXML
    private javafx.scene.control.Label labelN64;
    @FXML
    private javafx.scene.control.Label labelSNES;
    @FXML
    private javafx.scene.control.Label labelNES;


    @FXML
    private CheckBox startInFullscreenCheckBox;
    private Stage dialogStage;
    private PlatformSettingsWrapper settings;
    private MainApp mainApp;

    @FXML
    private CheckBox nintendoCheckbox;


    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public SettingsEditDialogController() {
        settings = new PlatformSettingsWrapper();
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }


    @FXML
    private void handleSaveButton() {
        mainApp.getAppSettings().setStartInFullscreenMode(startInFullscreenCheckBox.isSelected());
        mainApp.getAppSettings().setUseRetroArchForConsoles(nintendoCheckbox.isSelected());

        settings.getPlatformSettings("Steam").setPath(locationSteam.getText());
        settings.getPlatformSettings("Origin").setPath(locationOrigin.getText());
        settings.getPlatformSettings("Uplay").setPath(locationUplay.getText());
        settings.getPlatformSettings("GOG").setPath(locationGOG.getText());
        settings.getPlatformSettings("SNES").setPath(locationSNES.getText());
        settings.getPlatformSettings("NES").setPath(locationNES.getText());
        settings.getPlatformSettings("N64").setPath(locationN64.getText());
        settings.getPlatformSettings("Wii").setPath(locationWii.getText());
        settings.getPlatformSettings("GSM").setPath(locationGSM.getText());
        settings.getPlatformSettings("RetroArch").setPath(locationRetroArch.getText());
        settings.getPlatformSettings("Xbox").setPath(locationXbox.getText());
        settings.getPlatformSettings("Arc").setPath(locationArc.getText());
        settings.getPlatformSettings("Desura").setPath(locationDesura.getText());
        mainApp.savePreferences(settings);
    }

    @FXML
    private void handleDonation() {
        try {
            mainApp.getHostServices().showDocument("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=HEBSENXKHUC66");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    @FXML
    private void handleNintendoCheckBox() {
        if(nintendoCheckbox.isSelected()) {
            labelNES.setDisable(true);
            labelSNES.setDisable(true);
            labelN64.setDisable(true);
        } else {
            labelNES.setDisable(false);
            labelSNES.setDisable(false);
            labelN64.setDisable(false);
        }
    }

    @FXML
    private void handleFindSteam() {
        String path = findFile("Steam");
        locationSteam.setText(path);
        settings.setPlatformPath("Steam", path);
    }

    @FXML
    private void handleSteamCheckbox() {
        settings.getPlatformSettings("Steam").setFullscreen(steamFullscreenCheckbox.isSelected());
    }

    @FXML
    private void handleFindUplay() {
        String path = findFile("Uplay");
        locationUplay.setText(path);
        settings.setPlatformPath("Uplay", path);
    }

    @FXML
    private void handleFindOrigin() {
        String path = findFile("Origin");
        locationOrigin.setText(path);
        settings.setPlatformPath("Origin", path);
    }

    @FXML
    private void handleFindGOG() {
        String path = findFile("GOG");
        locationGOG.setText(path);
        settings.setPlatformPath("GOG", path);
    }

    @FXML
    private void handleFindNES() {
        String path = findFile("NES");
        locationNES.setText(path);
        settings.setPlatformPath("NES", path);
    }

    @FXML
    private void handleFindSNES() {
        String path = findFile("SNES");
        locationSNES.setText(path);
        settings.setPlatformPath("SNES", path);
    }

    @FXML
    private void handleFindN64() {
        String path = findFile("N64");
        locationN64.setText(path);
        settings.setPlatformPath("N64", path);
    }

    @FXML
    private void handleFindWii() {
        String path = findFile("Wii");
        locationWii.setText(path);
        settings.setPlatformPath("Wii", path);
    }

    @FXML
    private void handleFindGSM() {
        String path = findFile("GSM");
        locationGSM.setText(path);
        settings.setPlatformPath("GSM", path);
    }

    @FXML
    private void handleFindRetroArch() {
        String path = findFile("RetroArch");
        locationRetroArch.setText(path);
        settings.setPlatformPath("RetroArch", path);
    }

    @FXML
    private void handleRetroArchCheckbox() {
        settings.getPlatformSettings("RetroArch").setFullscreen(retroArchFullscreenCheckbox.isSelected());
    }

    @FXML
    private void handleFindXbox() {
        String path = findFile("Xbox");
        locationXbox.setText(path);
        settings.setPlatformPath("Xbox", path);
    }

    @FXML
    private void handleFindArc() {
        String path = findFile("Arc");
        locationArc.setText(path);
        settings.setPlatformPath("Arc", path);
    }

    @FXML
    private void handleFindDesura() {
        String path = findFile("Desura");
        locationDesura.setText(path);
        settings.setPlatformPath("Desura", path);
    }

    private String findFile(String name) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open " + name + ".exe");
        File file = fileChooser.showOpenDialog(dialogStage);

        if(file != null) return file.getAbsolutePath();
        else             return "";
    }

    public PlatformSettingsWrapper getSettings() {
        return settings;
    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        startInFullscreenCheckBox.setSelected(mainApp.getAppSettings().isStartInFullscreenMode());
        nintendoCheckbox.setSelected(mainApp.getAppSettings().isUseRetroArchForConsoles());
        handleNintendoCheckBox();
    }

    /**
     * Loads existing settings into GUI
     * @param settings
     */
    public void setSettings(PlatformSettingsWrapper settings, AppSettings appSettings) {
        this.settings = settings;
        PlatformSettings set;

        set = settings.getPlatformSettings("Steam");
        if(set != null) {
            this.locationSteam.setText(set.getPath());
            this.steamFullscreenCheckbox.setSelected(set.getFullscreen());
        }

        set = settings.getPlatformSettings("Uplay");
        if(set != null) {
            this.locationUplay.setText(set.getPath());
        }

        set = settings.getPlatformSettings("Origin");
        if(set != null) {
            this.locationOrigin.setText(set.getPath());
        }

        set = settings.getPlatformSettings("GOG");
        if(set != null) {
            this.locationGOG.setText(set.getPath());
        }

        set = settings.getPlatformSettings("NES");
        if(set != null) {
            this.locationNES.setText(set.getPath());
        }

        set = settings.getPlatformSettings("SNES");
        if(set != null) {
            this.locationSNES.setText(set.getPath());
        }

        set = settings.getPlatformSettings("N64");
        if(set != null) {
            this.locationN64.setText(set.getPath());
        }

        set = settings.getPlatformSettings("Wii");
        if(set != null) {
            this.locationWii.setText(set.getPath());
        }

        set = settings.getPlatformSettings("GSM");
        if(set != null) {
            this.locationGSM.setText(set.getPath());
        }

        set = settings.getPlatformSettings("RetroArch");
        if(set != null) {
            this.locationRetroArch.setText(set.getPath());
            this.retroArchFullscreenCheckbox.setSelected(set.getFullscreen());
        }

        set = settings.getPlatformSettings("Xbox");
        if(set != null) {
            this.locationXbox.setText(set.getPath());
        }

        set = settings.getPlatformSettings("Arc");
        if(set != null) {
            this.locationArc.setText(set.getPath());
        }

        set = settings.getPlatformSettings("Desura");
        if(set != null) {
            this.locationDesura.setText(set.getPath());
        }
    }

}
