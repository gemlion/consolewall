package consoleWall;

import consoleWall.model.AppSettings;
import consoleWall.model.PlatformSettings;
import consoleWall.model.PlatformSettingsWrapper;
import consoleWall.view.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

/**
 * The Main App for starting everything else, containing the primary stage
 */
public class MainApp extends Application {

    /* ................................................................................................ ATTRIBUTES .. */

    private Stage primaryStage;
    private BorderPane rootLayout;
    private PlatformSettingsWrapper settings; // Better observableList? // TODO
    private AppSettings appSettings;
    private ConsoleTileController tileController;
    private MainAppController mainAppController;


    /* .............................................................................................. CONSTRUCTORS .. */
    public MainApp() {
        this.settings = new PlatformSettingsWrapper();
    }

    /* ................................................................................................... METHODS .. */

    /**
     * Start method, run when the app starts
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        loadPreferences();
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Console Wall - Your Game Platform Starter");

        // Set the application icon.
        this.primaryStage.getIcons().add(new Image("resources/img/desktop_icon.png"));

        initRootLayout();
        showConsoleTiles();
        primaryStage.setMaximized(true);
        primaryStage.setFullScreen(appSettings.isStartInFullscreenMode());

    }

    /**
     * Initializes root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Give the controller access to the main app
            mainAppController = loader.getController();
            mainAppController.setMainApp(this);

            // Show scene containing root layout
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void savePreferences(PlatformSettingsWrapper settings) {
        System.out.println("Saving preferences ...");
        this.settings = settings;
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        prefs.putBoolean("fullscreen", appSettings.isStartInFullscreenMode());
        System.out.println("Start Fullscreen: " + this.appSettings.isStartInFullscreenMode());
        prefs.putBoolean("useRAforNintendo", appSettings.isUseRetroArchForConsoles());
        tileController.updateButtons();
        mainAppController.setMainApp(this);

        // Saving platform settings as XML
        savePlatformSettingsToFile(new File(System.getProperty("user.home") + "/.ConsoleWallSettings.xml"));

    }

    public void loadPreferences() {
        System.out.println("Loading preferences ...");
        loadPlatformSettingsFromFile(new File(System.getProperty("user.home") + "/.ConsoleWallSettings.xml"));
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        this.appSettings = new AppSettings(false, false);
        this.appSettings.setStartInFullscreenMode(prefs.getBoolean("fullscreen", false));
        this.appSettings.setUseRetroArchForConsoles(prefs.getBoolean("useRAforNintendo", false));
        System.out.println("Start Fullscreen: " + this.appSettings.isStartInFullscreenMode());
        System.out.println("Use RetroArch for Nintendo Platforms: " + this.appSettings.isUseRetroArchForConsoles());
    }


    /**
     * Method for loading the console tiles layout and inserting it into the root layout
     */
    public void showConsoleTiles() {
        try {
            // Load person overview
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ConsoleTilesLayout.fxml"));
            AnchorPane consoleTiles = (AnchorPane) loader.load();

            // Set person overview into center of root layout
            rootLayout.setCenter(consoleTiles);

            // Give the controller access to the main app
            tileController = loader.getController();
            tileController.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Opens the settings dialog
     */
    public void showSettingsEditDialog() {
        try {
            // Load fxml and create new stage for dialog
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/SettingsEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Settings");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller
            SettingsEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);
            controller.setSettings(settings, appSettings);

            // Show dialog
            dialogStage.showAndWait();

            settings = controller.getSettings();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(settings);
    }

    // XML Methods
    /**
     * Save ConsoleWall's settings to the given file
     * @param file
     */
    public void savePlatformSettingsToFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(PlatformSettingsWrapper.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Saving to file
            marshaller.marshal(settings, file);

        } catch (Exception e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Could not save data");
            alert.setContentText("Could not save data to file:\n" + file.getPath());
            alert.showAndWait();
        }
    }

    /**
     * Loads ConsoleWall's settings from .ConsoleWallSettings.xml, if it exists
     * @param file
     */
    public void loadPlatformSettingsFromFile(File file) {
        try {
            JAXBContext context = JAXBContext
                    .newInstance(PlatformSettingsWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            // Reading XML from the file and unmarshalling.
            this.settings = (PlatformSettingsWrapper) um.unmarshal(file);

        } catch (Exception e) { // catches ANY exception
            e.printStackTrace();
            System.err.println("Could not open ConsoleWallSettings.xml");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("New configuration");
            alert.setHeaderText("New Configuration");
            alert.setContentText("Created new file:\n" + file.getPath());
            alert.showAndWait();
            this.settings = new PlatformSettingsWrapper();
        }
    }

    public void toBack() {
        primaryStage.toBack();
    }
    public void toFront() {
        primaryStage.toFront();
    }


    /* ......................................................................................... GETTERS & SETTERS .. */
    public Stage getPrimaryStage() {
        return primaryStage;
    }


    public PlatformSettingsWrapper getSettings() {
        return this.settings;
    }

    public AppSettings getAppSettings() {
        return appSettings;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
