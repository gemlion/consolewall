package consoleWall.model;

import javafx.beans.property.*;

/**
 * Created by georg on 01.09.15.
 *
 * Storage container for video game platform settings
 */
public class PlatformSettings {

    /* ................................................................................................ ATTRIBUTES .. */

    private final String name;
    private final StringProperty path;
    private final BooleanProperty fullscreen;
    private boolean active;


    /* .............................................................................................. CONSTRUCTORS .. */
    /**
     * Default constructor.
     */
    public PlatformSettings() {
        this(null, null, false);
    }

    /**
     * Creates a new settings container for a video game platform
     * @param name          name of the platform, e.g. "Steam"
     * @param path          path to the platforms executable, e.g. "Steam.exe" or "Steam.sh"
     * @param fullscreen    whether the platform should be started in fullscreen mode (Steam only)
     */
    public PlatformSettings(String name, String path, boolean fullscreen) {
        System.out.println("Creating settings container for " + name);
        this.name = name;
        this.path = new SimpleStringProperty();
        setPath(path);
        this.fullscreen = new SimpleBooleanProperty(fullscreen);
    }

    public String getPath() {
        return path.get();
    }

    public StringProperty pathProperty() {
        return path;
    }

    public void setPath(String path) {
        if(path == null) this.active = false;
        else this.active = !path.isEmpty();
        this.path.set(path);
    }

    public boolean getFullscreen() {
        return fullscreen.get();
    }

    public BooleanProperty fullscreenProperty() {
        return fullscreen;
    }

    public void setFullscreen(boolean fullscreen) {
        this.fullscreen.set(fullscreen);
    }

    public String getName() {
        return name;
    }

    public String getCommand() {
        if(fullscreen.get())
            return path.get();
        else
            switch(name) {
                case "Steam": return path.get() + " " + PlatformCommands.STEAM_FULLSCREEN;
                default: return path.get();
            }
    }

    public boolean isActive() {
        return active;
    }
}
