package consoleWall.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by georg on 02.09.15.
 * Container for platform settings of all available video game platforms for storage in XML
 */
@XmlRootElement(name = "platformSettings")
public class PlatformSettingsWrapper {

    /* ................................................................................................ ATTRIBUTES .. */
    @XmlElement(name = "settings")
    private Map<String, PlatformSettings> settings; // Better observableList? // TODO

    /* .............................................................................................. CONSTRUCTORS .. */
    public PlatformSettingsWrapper() {
        this.settings = new HashMap<String, PlatformSettings>();
    }


    /* ................................................................................................... METHODS .. */

    /* ......................................................................................... GETTERS & SETTERS .. */

    public PlatformSettings getPlatformSettings(String platform) {
        if(!settings.containsKey(platform)) settings.put(platform, new PlatformSettings(platform, "", false));
        return settings.get(platform);
    }


    public void setPlatformPath(String platform, String path) {
        if(!settings.containsKey(platform))  settings.put(platform, new PlatformSettings(platform, path, false));
        settings.get(platform).setPath(path);

        for(Map.Entry<String, PlatformSettings> e : settings.entrySet()) {
            System.out.println(e.getValue().getPath());
        }
    }

}
