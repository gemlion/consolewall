package consoleWall.model;

/**
 * Created by georg on 02.09.15.
 *
 * Storage container for ConsoleWall's settings.
 */
public class AppSettings {

    /* ................................................................................................ ATTRIBUTES .. */
    private boolean startInFullscreenMode;
    private boolean useRetroArchForConsoles;


    /* .............................................................................................. CONSTRUCTORS .. */

    /**
     * Create a new container for ConsolWall's settings
     * @param fullscreen                whether ConsolWall should start fullscreen
     * @param useRetroArchForConsoles   whether RetroArch should be used for supported retro consoles
     */
    public AppSettings(boolean fullscreen, boolean useRetroArchForConsoles) {
        this.startInFullscreenMode      = fullscreen;
        this.useRetroArchForConsoles    = useRetroArchForConsoles;
    }

    /* ................................................................................................... METHODS .. */

    /* ......................................................................................... GETTERS & SETTERS .. */

    public boolean isStartInFullscreenMode() {
        return startInFullscreenMode;
    }

    public void setStartInFullscreenMode(boolean startInFullscreenMode) {
        this.startInFullscreenMode = startInFullscreenMode;
    }

    public boolean isUseRetroArchForConsoles() {
        return useRetroArchForConsoles;
    }

    public void setUseRetroArchForConsoles(boolean useRetroArchForConsoles) {
        this.useRetroArchForConsoles = useRetroArchForConsoles;
    }
}
