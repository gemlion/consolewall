package consoleWall.model;

/**
 * Created by georg on 02.09.15.
 */
public abstract class PlatformCommands {

    /* ................................................................................................ ATTRIBUTES .. */
    public static String STEAM = "steam://open/games/grid";
    public static String STEAM_FULLSCREEN = "steam://open/bigpicture";
    public static String STEAM_EXIT = "steam://ExitSteam";
    public static String ORIGIN = "";
    public static String UPLAY = "";
    public static String GOG = "";
    public static String NES = "";
    public static String N64 = "";
    public static String Wii = "";
    public static String SNES = "";
    public static String RETROARCH = "";
    public static String RETROARCH_FULLSCREEN = "";
    public static String RETROARCH_MENU = "";


    /* .............................................................................................. CONSTRUCTORS .. */
    
    /* ................................................................................................... METHODS .. */

    /* ......................................................................................... GETTERS & SETTERS .. */

}
